Here's the plan

1: Create a Loss function.

2: Put the loss function in a terminal model.

3: Get policy trajectory from ddp.xs and ddp.us for learning.
    
    1: No subsampling

4: Learn Policy and Loss:
    
    1: The first dataset is generated with crocoddyl

5: Use the neural networks to 

    1: Predict the loss in a terminal model
    
    2: Predict warmstart.
