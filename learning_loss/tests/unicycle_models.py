import numdifftools as nd
import numpy as np
import torch
import crocoddyl
from networks import CostNetwork 
from utils import loss_function





class LossModelUnicycle(crocoddyl.ActionModelAbstract):
    """
    This includes a feedforward network in crocoddyl
    
    """
    def __init__(self):
        crocoddyl.ActionModelAbstract.__init__(self, crocoddyl.StateVector(3), 2, 5)

    def calc(self, data, x, u=None):
        if u is None:
            u = self.unone
        
        cost = loss_function(x)
        data.cost = cost.item()
        print(f"Applying loss function on {x}, cost = {cost} \n")
    def calcDiff(self, data, x, u=None):
        if u is None:
            u = self.unone
        
        # solve automatic numerical differentiation
        dx = nd.Gradient(loss_function)
        dxx = nd.Hessian(loss_function)
        #print(dxx(x))
        #print(dxx)
        data.Lx = dx(x)
        data.Lxx = dxx(x)





class NetworkModelUnicycle(crocoddyl.ActionModelAbstract):
    """
    This includes a feedforward network in crocoddyl
    
    """
    def __init__(self, neural_net):
        crocoddyl.ActionModelAbstract.__init__(self, crocoddyl.StateVector(3), 2, 5)
        self.net = neural_net
        device = torch.device('cpu')
        self.net.to(device)

    def calc(self, data, x, u=None):
        if u is None:
            u = self.unone
        x = torch.Tensor(x).resize_(1, 3)
        
        with torch.no_grad():
            data.cost = self.net(x).item()
        print(f"Applying Network on {x}, prediction = {data.cost} \n")
    def calcDiff(self, data, x, u=None):
        if u is None:
            u = self.unone

        x = torch.Tensor(x).resize_(1, 3)
        
        data.Lx = self.net.jacobian(x).detach().numpy()
        data.Lxx = self.net.hessian(x).detach().numpy()        