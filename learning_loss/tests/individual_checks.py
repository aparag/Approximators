"""
Solve the same problem twice. Coldstart vs Warmstart


"""
import sys
sys.path.append("../src/")


try:
    import numpy as np
    import torch 
    import crocoddyl
    import matplotlib.pyplot as plt

    from unicycle_models import LossModelUnicycle
    from unicycle_models import NetworkModelUnicycle
    from networks import StatePolicyApproximator
    from networks import ControlPolicyApproximator
    from networks import CostNetwork
    from utils import cartesian_grid
    from utils import circular_data
    from utils import loss_function
except ImportError:
    print("Cannot import libraries")


# Load the three trained networks

costNet = torch.load("../src/neural_networks/costNet.pth")               # Neural net trained on Loss function
stateNet = torch.load("../src/neural_networks/stateNet.pth")             # Neural net trained on state policy
controlNet = torch.load("../src/neural_networks/controlNet.pth")         # Neural net trained on control policy

costNet.to('cpu')
stateNet.to('cpu')
controlNet.to('cpu')


# Starting Positions

data = cartesian_grid(1000)


idx             = np.random.choice(data.shape[0],1) 
x               = data[idx,:]         # Starting point [X, Y, Theta]

# Problem 1.

model             = crocoddyl.ActionModelUnicycle()   
model.costWeights = np.array([1.5, 1]).T
terminal_model    = LossModelUnicycle()                                                    # Terminal model with custom loss function
problem           = crocoddyl.ShootingProblem(x.T, [model] * 30, terminal_model)           
ddp               = crocoddyl.SolverDDP(problem)
log               = crocoddyl.CallbackLogger()

ddp.setCallbacks([log])
ddp.solve([], [],1)
xs = np.array(ddp.xs)
us = np.array(ddp.us)

"""
# Warmstart the same problem and add terminal model with neural network
warmstart_xs              = stateNet.warmstart_policy(torch.Tensor(x).to('cpu'))            # Warmstart xs
warmstart_us              = controlNet.warmstart_policy(torch.Tensor(x).to('cpu'))          # Warmstart us
terminal_model_unicycle   = NetworkModelUnicycle(costNet)                                   # Terminal model with neural network

model2                    = crocoddyl.ActionModelUnicycle()   
model2.costWeights        = np.array([1.5, 1]).T

problem2                  = crocoddyl.ShootingProblem(x.T, [model2] * 30, terminal_model_unicycle)  # This terminal model has neural net inside it that predicts the loss.

ddp2                      = crocoddyl.SolverDDP(problem2)

log2                      = crocoddyl.CallbackLogger()

ddp2.setCallbacks([log2])


ddp2.solve(warmstart_xs, warmstart_us,1000)

"""
# Terminal model with neural network
terminal_model_unicycle   = NetworkModelUnicycle(costNet)                                   # Terminal model with neural network

model3                    = crocoddyl.ActionModelUnicycle()   
model3.costWeights        = np.array([1.5, 1]).T

problem3                  = crocoddyl.ShootingProblem(x.T, [model3] * 30, terminal_model_unicycle)  # This terminal model has neural net inside it that predicts the loss.

ddp3                      = crocoddyl.SolverDDP(problem3)

log3                      = crocoddyl.CallbackLogger()

ddp3.setCallbacks([log3])


ddp3.solve([], [],1)

"""


# Warmstart the same problem and add terminal model with neural network
terminal_model            = LossModelUnicycle()                                                    # Terminal model with custom loss function
model4                    = crocoddyl.ActionModelUnicycle()   
model4.costWeights        = np.array([1.5, 1]).T

problem4                  = crocoddyl.ShootingProblem(x.T, [model4] * 30, terminal_model_unicycle)  # This terminal model has neural net inside it that predicts the loss.

ddp4                      = crocoddyl.SolverDDP(problem4)

log4                      = crocoddyl.CallbackLogger()

ddp4.setCallbacks([log4])


ddp4.solve(warmstart_xs, warmstart_us,1000)


plt.plot([np.linalg.norm(f) for f in log.fs],'-*',c = "blue", label = "ColdStart")
plt.plot([np.linalg.norm(f) for f in log2.fs],"-*",c = "grey", label = "Warmstart with terminal network")
plt.plot([np.linalg.norm(f) for f in log3.fs],"-*",c = "red", label = "No Warmstart.Just terminal network")
plt.plot([np.linalg.norm(f) for f in log4.fs],"-*",c = "green", label = "Just Warmstart.No terminal network")

plt.legend(loc = 'upper right')
plt.title("Norm of Gaps")
plt.ylabel("Norm of f in log.fs")
#plt.savefig("./images/gaps.png")
plt.show()




plt.clf()
plt.plot(log.costs,'-*',c = "blue", label = "ColdStart")
#plt.plot(log2.costs,"-*",c = "grey", label = "Warmstart with terminal network")
plt.plot(log3.costs,"-*",c = "red", label = "No Warmstart.Just terminal network")
#plt.plot(log4.costs,"-*",c = "green", label = "Just Warmstart.No terminal network")
plt.legend(loc = 'upper right')
plt.title("Cost")
plt.ylabel("log.cost")
#plt.savefig("./images/gaps.png")
plt.show()
"""


