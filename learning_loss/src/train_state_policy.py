
import numpy as np
import crocoddyl 
import torch
import torch.nn as nn
import numdifftools as nd
from tqdm import tqdm
from utils import cartesian_grid
from utils import loss_function
from networks import StatePolicyApproximator
from networks import ControlPolicyApproximator
from terminal_models import LossModelUnicycle
from terminal_models import NetworkModelUnicycle


torch.set_default_dtype(torch.float32)

if torch.cuda.is_available():
    torch.cuda.empty_cache()





# ... Hyperparams for State_PolicyApproximator

FC_DIMS               = [32, 32, 32]

ACTIVATION            = nn.ReLU()
DEVICE                = 'cuda'

# Training Hyperparams

EPOCHS                = 500
BATCHSIZE             = 128
LR                    = 5e-3
DECAY                 = 1e-8

HORIZON               = 30










# Generate data from Crocoddyl that has custom loss function inside it.


states = np.loadtxt("./dataset/state_xtrain.npy", delimiter = ",")

state_xs = np.loadtxt("./dataset/state_ytrain.npy", delimiter = ",")
print(states.shape, state_xs.shape)



# Training Dataset for stateApproximator

xtrain = torch.Tensor(states[0:49000,:])                           # to tensor
ytrain = torch.Tensor(state_xs[0:49000,:])                         # to tensor

# # ... Dataloader for training
dataset = torch.utils.data.TensorDataset(xtrain, ytrain)
dataloader = torch.utils.data.DataLoader(dataset, batch_size = BATCHSIZE, shuffle=True)

# Testing Data
xtest = torch.Tensor(states[49000:,:]).to(DEVICE)                          # to tensor
ytest = torch.Tensor(state_xs[49000:,:]).to(DEVICE)                        # to tensor

# Instantiate a stateApproximator

net = StatePolicyApproximator(input_dimensions=3, max_horizon=HORIZON, hidden_units=FC_DIMS, activation=ACTIVATION)
print(net)

# Training loss function
criterion1 = torch.nn.MSELoss()
criterion2 = torch.nn.L1Loss()

# Optimizer
opt = torch.optim.Adam(net.parameters(), lr = LR, betas=[0.9, 0.999], weight_decay=DECAY)

# Main training
net.to(DEVICE)
for epoch in range(EPOCHS):
    for data, target in dataloader:
        net.train()
        opt.zero_grad()

        data        = data.to(DEVICE)
        target      = target.to(DEVICE)
        output      = net(data)
        loss        = criterion1(output, target)
        loss.backward()
        opt.step()

    # Validation at the end of an epoch
    pred = net(xtest)
    mae = criterion2(pred, ytest)
    mse = criterion1(pred, ytest)
    print(f"EPOCH : {epoch} || MAE : {mae} || MSE : {mse}")


torch.save(net, './neural_networks/stateNet.pth')