"""This module implements three pytorch feedforward networks to learn cost, state and control policy"""
import numpy as np
import torch
import torch.nn as nn
torch.set_default_dtype(torch.float32)


class StatePolicyApproximator(nn.Module):

    def __init__(self, input_dimensions:int = 3, max_horizon:int = 50,
                hidden_units:list = [32, 32, 32], activation = nn.ReLU()):



        """Instantiate an untrained neural network with the given params
        
        Args
        ........
                
        :param      input_dimensions         = dimensions of q of the robot. state space  = [q, qdot].T 
                                                    q = [x, y, theta] for unicycle

        :param      max_horizon              = length of the horizon. Output dimension is 3 * max_horizon.

        :param      fc1_dims                 = number of units in the first fully connected layer. Default 32
        
        :param      fc2_dims                 = number of units in the second fully connected layer. Default 32

        :param      fc3_dims                 = number of units in the third fully connected layer. Default 32
        
        :param      activation               = activation for the layers, default ReLU().
    
        """
        super(StatePolicyApproximator, self).__init__()


        fc1_dims, fc2_dims, fc3_dims = hidden_units



        self.input_dimensions   = input_dimensions
        self.output_dimensions  = 3 * max_horizon
        self.fc1_dims           = fc1_dims
        self.fc2_dims           = fc2_dims
        self.fc3_dims           = fc3_dims
        self.activation_hidden  = activation

        # This structure seems to work.
        #........... Structure
        self.fc1 = nn.Linear(self.input_dimensions, self.fc1_dims)
        self.fc2 = nn.Linear(self.fc1_dims, self.fc2_dims)
        self.fc3 = nn.Linear(self.fc2_dims, self.fc3_dims)
        self.fc4 = nn.Linear(self.fc3_dims, self.output_dimensions)



        #........... Weight Initialization
        nn.init.kaiming_normal_(self.fc1.weight)
        nn.init.kaiming_normal_(self.fc2.weight)
        nn.init.kaiming_normal_(self.fc3.weight)
        nn.init.kaiming_normal_(self.fc4.weight)

        #........... Bias Initialization
        nn.init.constant_(self.fc1.bias, 0.001)
        nn.init.constant_(self.fc2.bias, 0.001)
        nn.init.constant_(self.fc3.bias, 0.001)
        nn.init.constant_(self.fc4.bias, 0.001)




    def forward(self, state):
        """
        The state trajectory learned by the neural network.
        
        """
        state_policy = self.activation_hidden(self.fc1(state))
        state_policy = self.activation_hidden(self.fc2(state_policy))
        state_policy = self.activation_hidden(self.fc3(state_policy))
        state_policy = self.fc4(state_policy)
        return state_policy

    @property
    def policy_shape(self):
        policy_length = int(self.output_dimensions)
        policy_shape0 = int(policy_length/3)
        return [policy_shape0, 3]

    def warmstart_policy(self, state):
        """
        Returns the guess for warmstarting in the format required by crocoddyl
        """
        init_xs = []
        init_xs.append(state.cpu().detach().numpy())
        init_xs = np.array(init_xs).reshape(1,3)
        shape = self.policy_shape
        with torch.no_grad():
            rollout = self.forward(state).cpu().detach().numpy().squeeze().reshape(*shape)
        init_xs = np.vstack((init_xs, rollout))
        return np.array(init_xs)




class ControlPolicyApproximator(nn.Module):
    def __init__(self, input_dimensions:int = 3, max_horizon:int = 20,
                hidden_units:list = [32, 32, 32], activation = nn.ReLU()):



        """Instantiate an untrained neural network with the given params
        
        Args
        ........
                
        :param      input_dimensions         = dimensions of q of the robot. State space  = [q, qdot].T 
                                                    q = [x, y, theta] for unicycle

        :param      max_horizon              = length of the horizon. Output dimension is 2 * max_horizon.

        :param      fc1_dims                 = number of units in the first fully connected layer. Default 32
        
        :param      fc2_dims                 = number of units in the second fully connected layer. Default 32

        :param      fc3_dims                 = number of units in the third fully connected layer. Default 32
        
        :param      activation               = activation for the layers, default ReLU().
    
        """
        super(ControlPolicyApproximator, self).__init__()


        fc1_dims, fc2_dims, fc3_dims = hidden_units



        self.input_dimensions   = input_dimensions
        self.output_dimensions  = 2 * max_horizon
        self.fc1_dims           = fc1_dims
        self.fc2_dims           = fc2_dims
        self.fc3_dims           = fc3_dims
        self.activation_hidden  = activation

        # This structure seems to work.
        #........... Structure
        self.fc1 = nn.Linear(self.input_dimensions, self.fc1_dims)
        self.fc2 = nn.Linear(self.fc1_dims, self.fc2_dims)
        self.fc3 = nn.Linear(self.fc2_dims, self.fc3_dims)
        self.fc4 = nn.Linear(self.fc3_dims, self.output_dimensions)



        #........... Weight Initialization
        nn.init.kaiming_normal_(self.fc1.weight)
        nn.init.kaiming_normal_(self.fc2.weight)
        nn.init.kaiming_normal_(self.fc3.weight)
        nn.init.kaiming_normal_(self.fc4.weight)

        #........... Bias Initialization
        nn.init.constant_(self.fc1.bias, 0.001)
        nn.init.constant_(self.fc2.bias, 0.001)
        nn.init.constant_(self.fc3.bias, 0.001)
        nn.init.constant_(self.fc4.bias, 0.001)




    def forward(self, state):
        """
        The state trajectory learned by the neural network.
        
        """
        state_policy = self.activation_hidden(self.fc1(state))
        state_policy = self.activation_hidden(self.fc2(state_policy))
        state_policy = self.activation_hidden(self.fc3(state_policy))
        state_policy = self.fc4(state_policy)
        return state_policy

    @property
    def policy_shape(self):
        policy_length = int(self.output_dimensions)
        policy_shape0 = int(policy_length/2)
        return [policy_shape0, 2]

    def warmstart_policy(self, state):
        """
        Returns the guess for warmstarting in the format required by crocoddyl
        """
        shape = self.policy_shape
        with torch.no_grad():
            rollout = self.forward(state).cpu().detach().numpy().squeeze().reshape(*shape)
        return np.array(rollout)


class CostNetwork(nn.Module):

    def __init__(self, 
                 input_dims:int = 3,
                 out_dims:int   = 1,
                 fc1_dims:int   = 20,
                 fc2_dims:int   = 26,
                 fc3_dims:int   = 2,
                 activation     = nn.Tanh()               
                ):
        super(CostNetwork, self).__init__()
        
        """
        Create a simple feedforward neural network with pytorch.
        
        Args
        ......
                1: input_dims  = input_features, i.e the number of features in the training dataset
                2: fc1_dims    = number of units in the first fully connected layer. Default 20
                3: fc2_dims    = number of units in the second fully connected layer. Default 20
                4: fc3_dims    = number of units in the third fully connected layer. Default 20
                5: activation  = activation for the layers, default tanh.
            
        Return
        ......
                1: A fully connected 3 hidden layered neural network
            
            
       
        """
        
        self.input_dims = input_dims
        self.out_dims   = out_dims
        self.fc1_dims   = fc1_dims
        self.fc2_dims   = fc2_dims
        self.fc3_dims   = fc3_dims
        self.activation = activation
        
        #........... Structure
        self.fc1 = nn.Linear(self.input_dims, self.fc1_dims)
        self.fc2 = nn.Linear(self.fc1_dims, self.fc2_dims)
        self.fc3 = nn.Linear(self.fc2_dims, self.fc3_dims)
        self.fc4 = nn.Linear(self.fc3_dims, self.out_dims)

        #........... Weight Initialization protocol
        nn.init.xavier_uniform_(self.fc1.weight)
        nn.init.xavier_uniform_(self.fc2.weight)
        nn.init.xavier_uniform_(self.fc3.weight)
        nn.init.xavier_uniform_(self.fc4.weight)

        
        #........... Bias Initialization protocol
        nn.init.constant_(self.fc1.bias, 0.001)
        nn.init.constant_(self.fc2.bias, 0.001)
        nn.init.constant_(self.fc3.bias, 0.001)
        nn.init.constant_(self.fc4.bias, 0.001)
        

        
    def forward(self, state):
        """
        The Value function predicted by the neural net. 
        
        """
        value = self.activation(self.fc1(state))
        value = self.activation(self.fc2(value))
        value = self.activation(self.fc3(value))
        value = self.fc4(value)
        
        return value 
    
    def jacobian(self, state):
        """
        Args
        ......
                1: x = state
            
        Returns
        .......
                1: The jacobian of the Value function with respect to state. Jacobian = dV/dx
        
        """
        return torch.autograd.functional.jacobian(self.forward, state).detach().squeeze()
    
    def hessian(self, state):
        """
        Args
        ......
                1: x = state
        Returns
        .......
            
                1: The hessian of the Value function with respect to state. Hessian = d^2V/dx^2        
        """
        return torch.autograd.functional.hessian(self.forward, state).detach().squeeze()
        

    def batch_jacobian(self, states):
        """
        Returns the jacobians of multiple inputs
        """
        j = [torch.autograd.functional.jacobian(self.forward, state).detach().squeeze() for state in states]
        return torch.stack(j).squeeze()
    
    def batch_hessian(self, states):

        """
        Returns the hessians of the multiple inputs 
        
        """
        h = [torch.autograd.functional.hessian(self.forward, state).detach().squeeze() for state in states]
        return torch.stack(h).squeeze()




