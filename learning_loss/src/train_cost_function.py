import numpy as np
import crocoddyl 
import torch
import torch.nn as nn
import numdifftools as nd

from utils import cartesian_grid
from utils import loss_function
from networks import CostNetwork

torch.set_default_dtype(torch.float32)

if torch.cuda.is_available():
    torch.cuda.empty_cache()



# ... Hyperparams for Cost Network

FC1_DIMS              = 30
FC2_DIMS              = 30
FC3_DIMS              = 30

ACTIVATION            = nn.Tanh()
DEVICE                = 'cuda'

# Training Hyperparams
DATASET_SIZE          = 49000

EPOCHS                = 500
BATCHSIZE             = 128
LR                    = 5e-3
DECAY                 = 1e-8






# Training Data
xtrain = cartesian_grid(n_points=DATASET_SIZE)        # np.array
ytrain = loss_function(xtrain)                        # np.array
xtrain = torch.Tensor(xtrain)                         # to tensor
ytrain = torch.Tensor(ytrain)                         # to tensor


# # ... Dataloader for training
dataset = torch.utils.data.TensorDataset(xtrain, ytrain)
dataloader = torch.utils.data.DataLoader(dataset, batch_size = BATCHSIZE, shuffle=True)

# Testing Data
xtest  = cartesian_grid(1000)
ytest  = loss_function(xtest)
xtest = torch.Tensor(xtest).to(DEVICE)              # to tensor
ytest = torch.Tensor(ytest).to(DEVICE)              # to tensor

# Initialize the cost network
net = CostNetwork(fc1_dims=FC1_DIMS, fc2_dims=FC2_DIMS, fc3_dims=FC3_DIMS)
print(net)

# Training loss function
criterion1 = torch.nn.MSELoss()
criterion2 = torch.nn.L1Loss()

# Optimizer
opt = torch.optim.Adam(net.parameters(), lr = LR, betas=[0.9, 0.999], weight_decay=DECAY)

# Main training
net.to(DEVICE)
for epoch in range(EPOCHS):
    for data, target in dataloader:
        net.train()
        opt.zero_grad()

        data        = data.to(DEVICE)
        target      = target.to(DEVICE)
        output      = net(data)
        loss        = criterion1(output, target)
        loss.backward()
        opt.step()
    
    # Validation at the end of an epoch
    acc = 0
    acc2 = 0
    acc3 = 0
    pred = net(xtest)
    mae = criterion2(pred, ytest)

    for i in range(len(xtest)):
        xtest[i].resize_(1, 3)
        pred = net(xtest[i]).item()
        if np.abs(pred - ytest[i].item()) < 0.1:
            acc += 1
        elif np.abs(pred - ytest[i].item()) < 0.5:
            acc2 += 1
        elif  np.abs(pred - ytest[i].item()) > 1.:
            acc3 += 1
    print(f"EPOCH : {epoch} || MAE : {mae} || Within 0.1: {acc}/{len(xtest)} || Within 0.5 : {acc2}/{len(xtest)} \
            || Greater than 1.0 : {acc3}/{len(xtest)}")


torch.save(net, './neural_networks/costNet.pth')