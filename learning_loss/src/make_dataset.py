import numpy as np
import crocoddyl
from utils import cartesian_grid
from terminal_models import LossModelUnicycle
from tqdm import tqdm


DATASET_SIZE = 50000
HORIZON      = 30

xtrain = cartesian_grid(n_points=DATASET_SIZE)        

control_ytrain = []
state_ytrain = []
for x in tqdm(xtrain):
    model = crocoddyl.ActionModelUnicycle()
    terminal_model = LossModelUnicycle()
    model.costweights = np.array([1.5, 1.]).T
    problem = crocoddyl.ShootingProblem(x.T, [model]*HORIZON, terminal_model)
    ddp = crocoddyl.SolverDDP(problem)
    ddp.solve([], [] , 1000)
    us = np.array(ddp.us)
    xs = np.array(ddp.xs[1:])

    control_ytrain.append(us.flatten())
    state_ytrain.append(xs.flatten())

control_ytrain = np.array(control_ytrain)
state_ytrain   = np.array(state_ytrain)

np.savetxt("state_xtrain.npy", xtrain, delimiter=',')
np.savetxt( "control_ytrain.npy", control_ytrain, delimiter=',')
np.savetxt("state_ytrain.npy", state_ytrain , delimiter=',')