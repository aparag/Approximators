import numpy as np
import torch



def cartesian_grid(n_points:int = 500):
    
    """Draw a collection of grid and random points of the given size
    The ratio of equidistant grid points and random points is 4:1
    i,e if n_points = 1500, then 1/4th of these points will be randomly generated 
    while the remaining 3/4th points are equidistant grid points.
    """
    
    try:
        n_points = np.abs(np.int(n_points))
        assert n_points > 0 

        if n_points >= 4 :
            n_grid_points = 3 *int(n_points / 4)
            grid_spacing  = int(np.cbrt(n_grid_points))
            xy_range = np.linspace(-2,2,num = grid_spacing, endpoint=True)
            theta_range = np.linspace(-np.pi, np.pi, num=grid_spacing, endpoint=True)
            
            grid = np.array([[x, y, z] for x in xy_range for y in xy_range for z in theta_range])
            n_random_points = int(n_points - grid.shape[0])
            
            random_points = []
            for _ in range(n_random_points):
                point = [np.random.uniform(-2., 2), np.random.uniform(-2., 2), np.random.uniform(-np.pi, np.pi)]
                random_points.append(point)
            
            random_points = np.array(random_points)
            
            data = np.vstack((grid, random_points))
            np.random.shuffle(data)
            del random_points, grid
            return np.array(data)
        elif n_points >=1 and n_points <= 3:
            data = []
            for _ in range(n_points):
                point = [np.random.uniform(-2., 2), np.random.uniform(-2., 2), np.random.uniform(-np.pi, np.pi)]
                data.append(point)            
            data = np.array(data)
            return np.array(data)
    except ValueError:
        print("No idea")




def loss_function(state):
    """
    Implement the loss function shown in README.md

    ARGS
    .......
    
    :param          state       = 3d vector. Atleast 1-d.

    RETURNS
    .......

    :return        loss     = float.
    
    """
    if isinstance(state, torch.Tensor):
        state = state.numpy()

    state = np.atleast_2d(state)
    loss = []
    for x in state:
        l = (np.sqrt(x[0]**2 + x[1]**2) - 1)**2 + (x[2] - np.arctan2(x[1], x[0]))**2 + \
            (np.cos(x[2]) - x[1])**2 + (np.sin(x[2]) - x[0])**2
        loss.append(l)

    return np.array(loss).squeeze().reshape(state.shape[0], 1)


def circular_data(r=[2], n=[100]):
    """
    @params:
        r = list of radii
        n = list of number of points required from each radii

    @returns:
        array of points from the circumference of circle of radius r centered on origin

    Usage: circle_points([2, 1, 3], [100, 20, 40]). This will return 100, 20, 40 points from
           circles of radius 2, 1 and 3
    """

    print(f" Returning {n} points from the circumference of a circle of radii {r}")

    circles = []
    for r, n in zip(r, n):
        t = np.linspace(0, 2* np.pi, n)
        x = r * np.cos(t)
        y = r * np.sin(t)
        z = np.zeros(x.size,)
        circles.append(np.c_[x, y, z])
    return np.array(circles).squeeze()



if __name__=='__main__':

    # Let's see what the loss function looks like
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    
    grid_points = cartesian_grid(n_points=10000)
    costs = loss_function(grid_points)


    """
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    im = ax.scatter(grid_points[:,0],grid_points[:,1],grid_points[:,2],cmap='hot',c=costs)

    ax.set_xlabel('<--  x  -->')
    ax.set_ylabel('<-- y -->')
    ax.set_zlabel('<-- z -->')
    plt.colorbar(im).set_label("Loss")
    plt.show()
    """
    plt.clf()
    plt.figure(figsize=(3.5, 3))

    plt.scatter(grid_points[:,0],grid_points[:,1],cmap='viridis',c=costs)
    plt.xlabel('<--  x  -->')
    plt.ylabel('<-- y -->')
    plt.colorbar().set_label("Loss")
    plt.show()