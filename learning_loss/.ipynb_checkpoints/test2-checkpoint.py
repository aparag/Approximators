import numpy as np
import torch 
import crocoddyl
import matplotlib.pyplot as plt

from terminal_models import LossModelUnicycle
from terminal_models import NetworkModelUnicycle
from networks import StatePolicyApproximator
from networks import ControlPolicyApproximator
from networks import CostNetwork
from utils import cartesian_grid
from utils import circular_data
from utils import loss_function

costNet = torch.load("./neural_networks/costNet.pth")        # Neural net trained on Loss function
stateNet = torch.load("./neural_networks/stateNet.pth")      # Neural net trained on state policy
controlNet = torch.load("./neural_networks/controlNet.pth")  # Neural net trained on control policy

costNet.to('cpu')
stateNet.to('cpu')
controlNet.to('cpu')




### Let's see how well the neural network (the cost network) has learned the cost.

data = cartesian_grid(10000)

predicted_cost = costNet(torch.Tensor(data)).cpu().detach().numpy().squeeze().reshape(10000, 1)
loss_calculated_by_loss_function = loss_function(data)
error = predicted_cost - loss_calculated_by_loss_function



plt.clf()
plt.scatter(data[:,0], data[:,1], c = error)
plt.colorbar().set_label('Error')
plt.xlabel("<-- X -->")
plt.ylabel("<-- Y -->")
plt.title("Error")
plt.savefig("mae.png")
plt.show()






### Let's see how well the StatePolicyApproximator has learned the state trajectory


error = []
for x in data[0:1000,:]:
    model             = crocoddyl.ActionModelUnicycle()   
    model.costWeights = np.array([1.5, 1]).T
    terminal_model    = LossModelUnicycle()
    problem           = crocoddyl.ShootingProblem(x.T, [model] * 50, terminal_model)
    ddp               = crocoddyl.SolverDDP(problem)
    ddp.solve()
    xs                = np.array(ddp.xs)
    
    pred              = stateNet.warmstart_policy(torch.Tensor(x).to('cpu'))
    error.append(np.mean((pred - xs)**2))

plt.clf()
plt.scatter(data[0:1000,0], data[0:1000,1], c = error)
plt.colorbar().set_label('Error')
plt.xlabel("<-- X -->")
plt.ylabel("<-- Y -->")
plt.title("MSE Error between warmstart policy and ddp.xs")
plt.savefig("mse.png")
plt.show()
