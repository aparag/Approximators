
import numpy as np
import crocoddyl 
import torch
import torch.nn as nn
import numdifftools as nd
from tqdm import tqdm
from utils import cartesian_grid
from utils import loss_function
from networks import StatePolicyApproximator
from networks import ControlPolicyApproximator
from terminal_models import LossModelUnicycle
from terminal_models import NetworkModelUnicycle


torch.set_default_dtype(torch.float32)

if torch.cuda.is_available():
    torch.cuda.empty_cache()





# ... Hyperparams for State_PolicyApproximator

FC_DIMS               = [32, 32, 32]

ACTIVATION            = nn.ReLU()
DEVICE                = 'cuda'

# Training Hyperparams
DATASET_SIZE          = 5000

EPOCHS                = 1500
BATCHSIZE             = 100
LR                    = 5e-3
DECAY                 = 1e-8

HORIZON               = 50










# Generate data from Crocoddyl that has custom loss function inside it.

xtrain = cartesian_grid(n_points=DATASET_SIZE)        # np.array

ytrain = []

for x in tqdm(xtrain):
    model = crocoddyl.ActionModelUnicycle()
    terminal_model = LossModelUnicycle()
    model.costweights = np.array([1.5, 1.]).T
    problem = crocoddyl.ShootingProblem(x.T, [model]*HORIZON, terminal_model)
    ddp = crocoddyl.SolverDDP(problem)
    ddp.solve([], [] , 1000)
    us = np.array(ddp.us)

    ytrain.append(us.flatten())

ytrain = np.array(ytrain)

# Training Dataset for stateApproximator

xtrain = torch.Tensor(xtrain)                         # to tensor
ytrain = torch.Tensor(ytrain)                         # to tensor

# # ... Dataloader for training
dataset = torch.utils.data.TensorDataset(xtrain, ytrain)
dataloader = torch.utils.data.DataLoader(dataset, batch_size = BATCHSIZE, shuffle=True)

# Testing Data
xtest  = cartesian_grid(100)
ytest  = []
for x in tqdm(xtest):
    model = crocoddyl.ActionModelUnicycle()
    terminal_model = LossModelUnicycle()
    model.costweights = np.array([1.5, 1.]).T
    problem = crocoddyl.ShootingProblem(x.T, [model]*HORIZON, terminal_model)
    ddp = crocoddyl.SolverDDP(problem)
    ddp.solve([], [] , 1000)
    us = np.array(ddp.us)
    ytest.append(us.flatten())

ytest = np.array(ytest)
xtest = torch.Tensor(xtest).to(DEVICE)              # to tensor
ytest = torch.Tensor(ytest).to(DEVICE)              # to tensor

# Instantiate a constrolApproximator

net = ControlPolicyApproximator(input_dimensions=3, max_horizon=HORIZON, hidden_units=FC_DIMS, activation=ACTIVATION)
print(net)

# Training loss function
criterion1 = torch.nn.MSELoss()
criterion2 = torch.nn.L1Loss()

# Optimizer
opt = torch.optim.Adam(net.parameters(), lr = LR, betas=[0.9, 0.999], weight_decay=DECAY)

# Main training
net.to(DEVICE)
for epoch in range(EPOCHS):
    for data, target in dataloader:
        net.train()
        opt.zero_grad()

        data        = data.to(DEVICE)
        target      = target.to(DEVICE)
        output      = net(data)
        loss        = criterion1(output, target)
        loss.backward()
        opt.step()

    # Validation at the end of an epoch
    pred = net(xtest)
    mae = criterion2(pred, ytest)
    mse = criterion1(pred, ytest)
    print(f"EPOCH : {epoch} || MAE : {mae} || MSE : {mse}")


torch.save(net, './neural_networks/controlNet.pth')